This is the first draft of my UNOFFICIAL Kult 4E system for Foundry VTT. It's based on copyrighted material, but contains no copyrighted material. You will be responsible, as the user, for inputting the material from your own copy of the Kult Divinity Lost book. 

It should be noted that I'm new to Foundry systems development, so not everything is completely optimized. I made this because I wanted to use it, and often took the "easy way out" of problems. If you have feature requests or want to contribute code, please let me know.

Clicking on the name of a move/advantage/disadvantage on the character sheet will allow you to roll (each item has an attribute that can be manually associated with it on the item sheet). Advantages and Disadvantages can be set to either Active (able to be rolled) or Passive (not able to be rolled) based on whether or not they trigger a move.

The two fields at the top of the sheet are intended to add ongoing or one-time modifiers to rolls. The bonus from aiding/hindering should be added here, as should penalties for major or critical wounds (these would likely be added into the "Forward" field). Penalties/Bonuses to "Keep It Together" and "See Through the Illusion" based on Stability are automatically factored in.

The Stability field should reflect the amount of Stability DAMAGE you have taken, rather than the amount of remaining stability. It is on my list to add an indicator for the description of your current Stabilty level and any penalties that will be applied.

Things I know do not currently work (as of version 0.37):

<ul>
<li>Wound Tracking (should be done manually through the "Situational Modifiers" field)</li>
<li>All kinds of visual design on the actor sheet (I'm kind of terrible at both graphic design and CSS)</li>
<li>Error checking on many fields (please let me know if you have questions about what type of data should be in each field)</li>
<li>Ammo Tracking on ranged weapons (my current solution is to use the Edit button on the weapon item to open its sheet so that the ammo can be updated)</li>
<li>Enhanced formatting for the move outputs (currently, you can put raw html in the "15+", "10-14" and "0=9" fields to output correctly in the chat log)</li></ul>

<p>“This product is unofficial Fan Content for KULT: Divinity Lost permitted under the <a href="https://helmgast.se/en/meta/fan-content-policy">Helmgast Fan Content Policy</a>"</p>