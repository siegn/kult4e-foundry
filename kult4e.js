import kult4eitemsheet from "./modules/sheets/kult4eitemsheet.js";
import kult4ePCsheet from "./modules/sheets/kult4ePCsheet.js";

async function preloadHandlebarTemplates() {
    const templatepaths =["systems/kult4e/templates/partials/move-card.hbs", "systems/kult4e/templates/partials/relationship-card.hbs", "systems/kult4e/templates/partials/weapon-card.hbs"
    ];
    return loadTemplates(templatepaths);
};

Hooks.once("init", function() {
console.log("Initializing Kult 4E");
Items.unregisterSheet("core", ItemSheet);
//Actors.unregisterSheet("core", ActorSheet);
Items.registerSheet("kult4e", kult4eitemsheet, {makeDefault: true});
Actors.registerSheet("kult4e", kult4ePCsheet, {makeDefault: true});

preloadHandlebarTemplates();
});